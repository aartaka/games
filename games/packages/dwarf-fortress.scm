;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Artyom Bologov <aartaka@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages dwarf-fortress)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (ice-9 match))

(define-public dwarf-fortress
  (let ((major-version "0")
        (minor-version "47")
        (patch-version "04"))
    (package
      (name "dwarf-fortress")
      (version (string-append major-version "." minor-version "." patch-version))
      (source (origin (method url-fetch)
                      (uri (string-append "https://www.bay12games.com/dwarves/df"
                                          "_" minor-version "_" patch-version "_"
                                          "linux.tar.bz2"))
                      (sha256
                       (base32 "1ri82c5hja6n0wv538srf2nbcyb8ip49w4l201m90cmcycmqgr8x"))))
      (build-system binary-build-system)
      (arguments
       `(#:patchelf-plan
         `(("libs/Dwarf_Fortress"
            ("sdl" "libc" "gcc:lib" "glib" "zlib" "glu" "gtk+-2" "libx11"
             "out"))
           ("libs/libgraphics.so"
            ("sdl" "sdl-ttf" "sdl-image"
             "libc" "gcc:lib" "glib" "zlib" "glu" "gtk+-2" "libx11")))
         #:install-plan
         `(("libs/Dwarf_Fortress" "Dwarf_Fortress")
           ("libs/libgraphics.so" "lib/libgraphics.so")
           ("data/" "data/")
           ("g_src/" "g_src/")
           ("raw/" "raw/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'make-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (use-modules (guix build utils))
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin"))
                      (bash (string-append (assoc-ref inputs "bash") "/bin/bash"))
                      (binary (string-append out "/Dwarf_Fortress"))
                      (audio-lib (string-append
                                  (assoc-ref inputs "openal")
                                  "/lib"))
                      (local-df-dir ".local/share/dwarf-fortress"))
                 (mkdir-p bin)
                 (with-directory-excursion bin
                   (call-with-output-file "dwarf-fortress"
                     (lambda (p)
                       (format p "#!~a
RUNDIR=$(mktemp -d)
LOCAL_DF_DIR=\"$HOME/~a\"
export LD_LIBRARY_PATH=~a${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH
chmod -R +w $RUNDIR $LOCAL_DF_DIR
cp -rv \"~a\"/data/* \"$LOCAL_DF_DIR\"/data
cp -rv \"~a\"/* \"$RUNDIR\"
cp -rv \"$RUNDIR\"/data/* \"$LOCAL_DF_DIR\"/data
cd \"$RUNDIR\"
chmod -R +w $RUNDIR $LOCAL_DF_DIR
rm -r data
ln -sfv \"$LOCAL_DF_DIR\"/data data
exec ~a \"$@\" " bash local-df-dir audio-lib out out binary)))
                   (chmod "dwarf-fortress" #o555)
                   #t))))
           (add-after 'install 'make-desktop-entry
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (make-desktop-entry-file
                  (string-append out "/share/applications/dwarf-fortress.desktop")
                  #:name "Slaves to Armok II: Dwarf Fortress"
                  #:exec (string-append out "/bin/dwarf-fortress")
                  #:categories '("Application" "Game")))
               #t)))))
      (inputs `(("bash" ,bash)
                ("sdl" ,sdl)
                ("sdl-ttf" ,sdl-ttf)
                ("sdl-image" ,sdl-image)
                ("gcc:lib" ,gcc "lib")
                ("glib" ,glib)
                ("gtk+-2" ,gtk+-2)
                ("glew" ,glew)
                ("glu" ,glu)
                ("libx11" ,libx11)
                ("zlib" ,zlib)
                ("openal" ,openal)))
      (synopsis "Dwarf Fortress is a single-player fantasy game. You can control a dwarven outpost or an adventurer in a randomly generated, persistent world.")
      (description "@itemize
@item Not just generated geometry -- a whole simulated world. Generated rise and fall of civilizations, personalities, creatures, cultures, etc. Infinite hours of gameplay.
@item A lifetime “living” project - created/updated since 2003, with no end in sight
@item Generate your unique world and manage a bustling colony of dwarves, even as they probably mine towards their eventual demise.
@item Optional ADVENTURER MODE: explore the generated world as a single hero in an RPG
@item Optional LEGENDS MODE: read the history of the generated world and your games in it
@item A new endless hobby, just for you!
@end itemize\n")
      (home-page "https://www.bay12games.com/dwarves/")
      ;; TODO: Add freeware license?
      (license (undistributable "Freeware License")))))
